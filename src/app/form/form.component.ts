import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  ValidationErrors,
  Validators,
} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { HttpService } from '../http.service';
import { Country } from '../models/country.interface';
import { COUNTRY_FORM, FLAGS } from '../models/models';

@Component({
  selector: 'sda-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class FormComponent implements OnInit {
  countryForm: FormGroup;
  arrayOfFlags = FLAGS;
  isEditing: boolean = false;
  constructor(
    private service: HttpService,
    private fb: FormBuilder,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.createForm();
    this.route.params.subscribe((params) => {
      if (params.id) {
        this.isEditing = true;
        this.service.getCountry(params.id).subscribe((country: Country) => {
          this.countryForm.setValue(country); // only possible if backend and front-end models are the same
        });
      }
    });
  }

  createForm(): void {
    this.countryForm = new FormGroup({
      id: new FormControl(null),
      countryName: new FormControl(null, [
        Validators.required,
        Validators.minLength(3),
        this.forbiddenCountryName,
      ]),
      countryCapital: new FormControl(
        null,
        [],
        this.forbiddenCapitalName.bind(this)
      ),
      population: new FormControl(),
      flag: new FormControl(),
    });
    // ONE WAY OF CREATING A FORM
    // this.countryForm = this.fb.group({...COUNTRY_FORM})
  }

  forbiddenCountryName(country: FormControl): ValidationErrors | null {
    if (country?.value?.toLowerCase() === 'china') {
      return { forbiddenCountryName: true };
    } else {
      return null;
    }
  }

  forbiddenCapitalName(capital: FormControl): Promise<ValidationErrors | null> {
    const promise = new Promise<ValidationErrors | null>((resolve, reject) => {
      setTimeout(() => {
        if (capital?.value?.toLowerCase() === 'beijing') {
          resolve({ forbiddenCapital: true });
        } else {
          resolve(null);
        }
      }, 2000);
    });
    return promise;
  }

  onSubmit(): void {
    if (this.countryForm.valid) {
      if (this.isEditing) {
        this.service.editCountry(this.countryForm.value).subscribe(() => {});
      } else {
        this.service.addCountry(this.countryForm.value).subscribe(() => {
          this.countryForm.reset();
        });
      }
    } else {
      alert('Form is not valid');
    }
  }
}
