import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DefGuard } from './def.guard';
import { FormComponent } from './form/form.component';
import { FunResolver } from './fun.resolver';
import { LoginComponent } from './login/login.component';
import { PlaygroundComponent } from './playground/playground.component';
import { TableComponent } from './table/table.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  {
    path: 'table',
    component: TableComponent,
  },
  {
    path: 'form',
    component: FormComponent,
    canActivate: [DefGuard],
  },
  {path: 'form/add', component: FormComponent},
  {path: 'form/edit/:id', component: FormComponent},
  {
    path: 'playground',
    component: PlaygroundComponent,
    resolve: {
      pic: FunResolver
    },
  },
  { path: '**', redirectTo: 'login' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
