import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StateService {
  private readonly adminName = 'admin';
  private readonly adminPassword = 'password';
 canActivate: boolean = false;
  constructor() { }
  canActivateFn(email: string, password: string): boolean {
    return this.canActivate = email === this.adminName && password === this.adminPassword;
  }
}