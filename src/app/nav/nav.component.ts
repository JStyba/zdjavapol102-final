import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { HttpService } from '../http.service';

@Component({
  selector: 'sda-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss'],
})
export class NavComponent implements OnInit {
@Input() title: string = 'SDA';
@Output() tabChangeEvent = new EventEmitter<number>();
styleOn: boolean = false;
  constructor() { }

  ngOnInit(): void {
  }

  onClick(tabNo: number): void {
  this.tabChangeEvent.emit(tabNo)
  }

}
