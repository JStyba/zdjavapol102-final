import { Component } from '@angular/core';

@Component({
  selector: 'sda-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  parentTitle = 'zdjavapol102';
  tabNo: number;
}
