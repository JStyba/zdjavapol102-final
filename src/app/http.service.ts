import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Country } from './models/country.interface';

@Injectable()
export class HttpService {

  constructor(private http: HttpClient) { }

  getCountries(): Observable<Country[]> {
    return this.http.get<Country[]>('http://localhost:8080/countries')
  }

  addCountry(country: Country): Observable<unknown> {
    return this.http.post('http://localhost:8080/countries', country)
  }

  getCountry(id: number): Observable<Country> {
    return this.http.get<Country>(`http://localhost:8080/country/${id}`)
  }

  editCountry(country: Country): Observable<unknown> {
    return this.http.put(`http://localhost:8080/countries`, country);
  }

  deleteCountry(id: number): Observable<unknown> {
    return this.http.delete(`http://localhost:8080/country/${id}`)
  }

  getMockData(): Observable<unknown> {
    return this.http.get('https://jsonplaceholder.typicode.com/users')
  }
}
