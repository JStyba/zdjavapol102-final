import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { Country } from '../models/country.interface';

@Component({
  selector: 'sda-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})
export class TableComponent implements OnInit {
someFn = (no: number) => {
  console.log(no)
}
countries: Country[];

  constructor(private service: HttpService) { }

  ngOnInit(): void {
    this.service.getCountries().subscribe((data:Country[]) => {
      this.countries = data;
    });
    // this.service.getMockData().subscribe(); MOCK DATA FROM API
  }

  deleteCountry(id: number): void {
    this.service.deleteCountry(id).subscribe(() => {
      this.service.getCountries().subscribe((data:Country[]) => {
        this.countries = data;
      });
    });
  }
}
