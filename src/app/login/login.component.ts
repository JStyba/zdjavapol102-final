import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { StateService } from '../state.service';

@Component({
  selector: 'sda-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: FormGroup;
  constructor(private stateService: StateService, private router: Router) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      email: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required, Validators.minLength(8)])
    });
  }

onSubmit() {
  console.log(this.stateService.canActivateFn(this.form.controls.email.value, this.form.controls.password.value))
if (this.stateService.canActivateFn(this.form.controls.email.value, this.form.controls.password.value)) {
  this.router.navigate(['/table'])
}
}

}
